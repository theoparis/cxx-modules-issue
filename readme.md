# CMake C++20 Modules Issue Reproduction

Requires cmake 3.28, ninja and clang.

```sh
cmake -B build -G Ninja -DCMAKE_CXX_COMPILER=clang++
```

It should give the following error:
```
-- The CXX compiler identification is Clang 18.0.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /home/theo/theos/build/install-host/bin/clang++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.1s)
CMake Error in CMakeLists.txt:
  Target "reproduction" has source file

    /home/theo/repro/./src/module.cpp

  in a "FILE_SET TYPE CXX_MODULES" but it is not scheduled for compilation.
```
