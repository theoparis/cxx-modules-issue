cmake_minimum_required(VERSION 3.28)
project(reproduction LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_library(
	reproduction
	./src/main.cpp
)
target_sources(
	reproduction
	PUBLIC
	FILE_SET CXX_MODULES FILES
	./src/module.cpp
)
target_compile_options(
	reproduction
	PUBLIC
	-Wall
	-Wextra
	-Wpedantic
	-Wconversion
	-Wsign-conversion
)

