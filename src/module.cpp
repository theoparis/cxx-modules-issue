export module module;

#include <iostream>

export namespace module {
export class Test {
public:
  Test() { std::cout << "Test::Test()" << std::endl; }
  ~Test() { std::cout << "Test::~Test()" << std::endl; }
};
} // namespace module
